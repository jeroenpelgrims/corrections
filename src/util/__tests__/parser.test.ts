import { parse, generateId } from '../parser';
import { Question } from '../types';

test('Empty string parses empty array', () => {
  expect(parse("")).toEqual([]);
});

test('Can parse multiple questions and corrections', () => {
  const input = `
    Question 1 (/5)
    - You did not provide a solution (-5)
    - You forgot the color (-1)

    Question 2 (/3)
    - You did not provide a solution 2 (-3)
    - You forgot the color 2 (-1)
  `;
  const output: Question[] = [
    {
      title: "Question 1",
      points: 5,
      corrections: [
        { feedback: "You did not provide a solution", deduction: 5 },
        { feedback: "You forgot the color", deduction: 1 }
      ]
    },
    {
      title: "Question 2",
      points: 3,
      corrections: [
        { feedback: "You did not provide a solution 2", deduction: 3 },
        { feedback: "You forgot the color 2", deduction: 1 }
      ]
    }
  ];

  expect(parse(input)).toEqual(output);
});

test('Can parse questions with decimal scores', () => {
  const input = `
    Question 1 (/5,5)
    - Foo (-1)
    Question 2 (/11.55)
    - Foo (-1)
  `;
  const output: Question[] = [
    {
      title: "Question 1",
      points: 5.5,
      corrections: [
        { feedback: "Foo", deduction: 1 }
      ]
    },
    {
      title: "Question 2",
      points: 11.55,
      corrections: [
        { feedback: "Foo", deduction: 1 }
      ]
    }
  ];

  expect(parse(input)).toEqual(output);
});

test('Can parse corrections with decimal scores', () => {
  const input = `
    Question 1 (/100)
    - Foo (-11.55)
    - Bar (-22,66)
  `;
  const output: Question[] = [
    {
      title: "Question 1",
      points: 100,
      corrections: [
        { feedback: "Foo", deduction: 11.55 },
        { feedback: "Bar", deduction: 22.66 }
      ]
    }
  ];

  expect(parse(input)).toEqual(output);
});

test('Can parse questions and corrections with special characters', () => {
  const input = `
    Question 1: clarification (and another) #1 (/1)
    - Correction #1: foo(bla) (-1)
  `;
  const output: Question[] = [
    {
      title: "Question 1: clarification (and another) #1",
      points: 1,
      corrections: [
        { feedback: "Correction #1: foo(bla)", deduction: 1 }
      ]
    }
  ];

  expect(parse(input)).toEqual(output);
});

test('Can parse multi line corrections', () => {
  const input = `
    Question 1 (/3)
    - Correction #1 (-1)
    - Multiple
      line and words
      correction (-2)
    - Correction #3 (-3)
    Question 2 (/1)
    - Correction #4 (-1)
  `;
  const output: Question[] = [
    {
      title: "Question 1",
      points: 3,
      corrections: [
        { feedback: "Correction #1", deduction: 1 },
        { feedback: `Multiple\nline and words\ncorrection`, deduction: 2 },
        { feedback: "Correction #3", deduction: 3 }
      ]
    },
    {
      title: "Question 2",
      points: 1,
      corrections: [
        { feedback: "Correction #4", deduction: 1 }
      ]
    }
  ];

  expect(parse(input)).toEqual(output);
});